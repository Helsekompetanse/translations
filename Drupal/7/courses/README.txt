What are these files?
=====================

The files contained in this folder are gettext files used for translating
Drupal 7 courses.

 * Read more about PO and POT files at Drupal.org.
   https://www.drupal.org/node/1814954

There is also a l10n_server.xml file that is used to
fake Drupal localization server instance. When adding new languages (PO files)
make sure to update the xml file accordingly.

 * Read more about l10n_server project here.
   https://www.drupal.org/project/l10n_server

How to modify language files
============================

It's recommended to use a recent version of Poedit when
making changes to the files in this folder.

Windows or Mac OS X version can be downloaded from: https://poedit.net/

Linux version is usually installed from the distribution's package manager.

How to fetch translations in Drupal 7 courses
=============================================

This is normally handled automatically (depending on configuration)
with l10n_update and hk_l10n_conf module.

Make sure the l10n_update configuration is correct in (outside scope of this README).
some-course.helsekompetanse.no/admin/config/regional/language/update

If the language files are updated but changes doesn't appear in a specific course,
it's possible to force an update through drush.

 $ drush sqlq 'UPDATE l10n_update_file SET last_checked="0" WHERE project="hk_l10n_conf"'
 $ drush cron
 $ drush cc all

How to extract general.pot file from course site
================================================

The following steps works fine in Linux based environment (Debian).

 1) Install requirements:

    Install msgmt executable, available in gettext package.

    ```
    $ sudo apt-get install gettext
    ```

    Install potx module in Drupal project.

    ```
    $ cd /some/root/path/site/domain
    $ drush en potx
    $ cd -
    ```

 2) Generate general.pot file.

    Command below assume you are standing in same directory as this README.

    ```
    $ ./generate_pot_file.sh /some/root/path/site/domain
    ```

 3) Move new POT file to git repository.

    Command below assume you are standing in same directory as this README.

    ```
    $ mv /some/root/path/general.pot .
    ```

 4) Commit changes in Git (this step is out of scope for this HOWTO).
