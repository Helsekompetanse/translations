#!/bin/bash
#
# Helper script to make reproducable POT files extracted from Drupal project.
#
# Usage: bash generate_port_file.sh <path>
#
# Where <path> is directory of site folder.
#
# vim:set ft=sh:

################################################################################
# Check if msgfmt executable is available or exit.
# Globals:
#  None.
# Arguments:
#  None.
# Returns:
#  None.
################################################################################
check_requirements_msgfmt() {
    if [[ ! -x "$(which msgfmt)" ]]; then
        echo "Msgfmt not found, exiting ..."
        echo
        echo "Hint for Debian 8, msgfmt is provided with \"gettext\" package."
        exit 1
    fi
}

################################################################################
# Set the variable for work path, directory of site folder.
# Exit 1 if given path doesn't appear to be correct, else assign variable two.
# Globals:
#  None.
# Arguments:
#  Raw input to script with path.
#  Variable to assign with work path.
# Returns:
#  None.
################################################################################
set_work_path() {
    local input="$1"
    local __work_path="$2"
    if [ ! -f "$input/settings.php" ]; then
        echo "Something went wrong!"
        echo
        echo "Please provide path to site folder:"
        echo "$ generate_pot_file.sh <path>"
        echo
        echo "Example:"
        echo "$ generate_pot_file.sh $HOME/public_html/platform_kurs/sites/kurs1.helsekompetanse.no"
        exit 1
    fi
    eval "$__work_path='$input'"
}

################################################################################
# Set variable with files where to extract translation strings from.
# Files are seperated with new line.
# Looks for php, module or inc file endings in Drupal modules (or themes)
# that belongs to Helsekompetanse package.
# Globals:
#  None.
# Arguments:
#  Variable to assign with string of file paths.
# Returns:
#  None.
################################################################################
get_files() {
    # Array with Helsekompetanse modules.
    declare -a modules=($(drush pm-list --status=enabled | grep '^ Helsekompetanse' | grep -Po '(?<=\().*?(?=\))' | sort -u | tr '\n' ' '))
    local result=""
    local __files="$1"

    for i in "${modules[@]}"; do
        echo "Searching: $i"
        local search_result="$(find -L ../../$(drush pm-info --fields=path "$i" | head -n1 | awk '{print $3}') \
            -type f \( -name "*.php" -o -iname "*.module" -o -iname "*.inc" -o -iname "*.info" \) \
            -exec readlink -f {} \;)"
        if [ -z "$search_result" ]; then
            echo No relevant files ...
        else
            echo $(echo "$search_result" | wc -l) files found ...
            if [ "$result" ]; then
                local result="$result"$'\n'"$search_result"
            else
                local result="$search_result"
            fi
        fi
    done

    local result="$(echo -e "$result" | sort -u)"

    echo
    echo A total of $(echo "$result" | wc -l) files found ...
    echo


    eval "$__files='$result'"
}

check_requirements_msgfmt

set_work_path "$1" WORK_PATH

ORIGINAL_PATH="$(pwd)"

cd "$WORK_PATH"

get_files FILES

#echo -e "$FILES" > /tmp/generate_pot_file_output.txt

FILES="$(echo -e "$FILES" | tr '\n' ',' | sed 's/,$//')" # Replace new line with ','

echo Initiatilize drush potx command ...
drush potx --files="$FILES"

GENERAL_POT_FILE="$(readlink -f ../../general.pot)"

if [ ! -f "$GENERAL_POT_FILE" ]; then
    echo
    echo "Abort!"
    echo "Could not find \"$GENERAL_POT_FILE\" ..."
    echo "Read the output above carefully."
    echo
    exit 1
fi

# Validate pot file. Step requires gettext package in Debian 8.
# If no output from command below, everything appear to be fine.
msgfmt -o /dev/null "$GENERAL_POT_FILE"

echo
echo "Generated POT file (move this file to your git folder):"
echo "$GENERAL_POT_FILE"
echo

cd "$ORIGINAL_PATH"
